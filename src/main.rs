#[macro_use]
extern crate clap;

use clap::{App, Arg};
use dicer;
use num_cpus;

fn main() {
    let matches = App::new("Dice Roller")
        .arg(
            Arg::with_name("rolls")
                .help("The number of times the dice should be rolled.")
                .short("n")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("threads")
                .help("The number of threads to use for throwing dice.")
                .short("t")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("dice")
                .help("The number of dice (and their dots) to roll (in the format 2d6)")
                .index(1),
        )
        .get_matches();

    let n = clap::value_t!(matches, "rolls", u64).unwrap_or(1_000_000);
    let threads = clap::value_t!(matches, "threads", u32).unwrap_or(num_cpus::get() as u32);
    let dice_type: Vec<&str> = matches
        .value_of("dice")
        .unwrap_or("3d7")
        .split('d')
        .collect();
    let n_dice: u32 = dice_type[0].parse().unwrap_or(3);
    let dots: u32 = dice_type[1].parse().unwrap_or(7);

    println!(
        "Throwing {} dice with {} dots each {} times (using {} threads):",
        n_dice, dots, n, threads
    );
    let throw_results = dicer::throw_dice_smt(n, n_dice, dots, threads);
    dicer::print_throws(throw_results);
}
