//! # Dicer
//!
//! This tool simulates the throwing of dice and prints a statistical overview of the results
//! # Command-line arguments
//!
//! * `-h, --help`: Print information on how to use this program.
//! * `-n`: The number of times the dice should be rolled (default: 1000000).
//! * `-t, --threads`: The number of threads to use for throwing dice (default: number of CPU
//!   cores).
//! * `dice` (positional): The number of dice (and their dots) to roll. The format is `2d6`: two
//!   dice with six dots each (default: 3d7).
//!
//! All arguments are optional. A simple `cargo run` will suffice and run with sensible defaults.

use rand::Rng;
use std::thread;

/// Prints the results of a dice simulation to the command-line (stdout).
///
/// # Arguments
///
/// * `throw_results`: A vector containing the results of a simulation. The index corresponds to
/// the sum of dots of a single throw, the value of the vector at that index corresponds to the
/// number of times that this sum of dots has occurred.
pub fn print_throws(throw_results: Vec<u64>) {
    let width = throw_results.len().to_string().len();
    let width2 = throw_results.iter().max().unwrap_or(&0).to_string().len();
    let n: u64 = throw_results.iter().sum();

    let mut cumulative_percentage = 0.0;
    for (i, value) in throw_results.iter().enumerate() {
        if *value != 0 {  // ignore throw results that never occurred, e. g. 0
            let percentage = *value as f64 / n as f64;
            cumulative_percentage += percentage;
            println!(
                "{: >width$}: [{: >width2$}, {: >7.4}, {: >8.4}]",
                i,
                value,
                percentage * 100.0,
                cumulative_percentage * 100.0,
                width = width,
                width2 = width2
            );
        }
    }
}

/// Performs the dice simulation for a single thread.
///
/// # Arguments
///
/// * `n`: The number of times the dice should be rolled. This should be the number of total dice
/// rolls divided by the number of threads.
/// * `n_dice`: The number of dice to roll.
/// * `dots`: The maximum dots that each die has.
///
/// # Returns
///
/// A vector containing the results of the simulation. The index corresponds to
/// the sum of dots of a single throw, the value of the vector at that index corresponds to the
/// number of times that this sum of dots has occurred.
pub fn throw_dice(n: u64, n_dice: u32, dots: u32) -> Vec<u32> {
    let mut throw_results = vec![0; (n_dice * dots + 1) as usize];
    let mut throw_result: u32;
    let mut rng = rand::thread_rng();

    for _ in 0..n {
        throw_result = 0;
        for _ in 0..n_dice {
            throw_result += rng.gen_range(1, dots + 1);
        }
        throw_results[throw_result as usize] += 1;
    }

    throw_results
}

/// Handles the dispatch of dice throws to worker threads.
///
/// # Arguments
///
/// * `n`: The number of times the dice should be rolled. This will be divided/distributed among the
/// threads.
/// * `n_dice`: The number of dice to roll.
/// * `dots`: The maximum dots that each die has.
/// * `threads`: The number of threads the simulation will be run on.
///
/// # Returns
///
/// A vector containing the results of the simulation. The index corresponds to
/// the sum of dots of a single throw, the value of the vector at that index corresponds to the
/// number of times that this sum of dots has occurred.
pub fn throw_dice_smt(n: u64, n_dice: u32, dots: u32, threads: u32) -> Vec<u64> {
    let mut handles = vec![];

    let mut ranges = Vec::with_capacity(threads as usize);
    for _ in 0..(threads - 1) {
        ranges.push(n / u64::from(threads));
    }
    ranges.push(n - ((u64::from(threads) - 1) * (n / u64::from(threads))));

    for i in 0..threads {
        let range = ranges[i as usize];
        let handle = thread::spawn(move || throw_dice(range, n_dice, dots));

        handles.push(handle);
    }

    let mut throw_results: Vec<u64> = vec![0; (n_dice * dots + 1) as usize];
    for handle in handles {
        let sub_result = handle.join().unwrap();
        for (i, value) in sub_result.iter().enumerate() {
            throw_results[i] += u64::from(*value)
        }
    }

    throw_results
}
