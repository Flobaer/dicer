# Usage

A simple `cargo run` (or executing the binary of `cargo build`) suffices.

The parameters can be adjusted as follows:


* `-n`: The number of times the dice should be rolled (default: 1000000).
* `-t, --threads`: The number of threads to use for throwing dice (default: number of CPU
  cores).
* `dice` (positional): The number of dice (and their dots) to roll. The format is `2d6`: two
  dice with six dots each (default: 3d7).

## Examples

```bash
# 1 billion dice throws on 4 threads with 8 dice (with 10 dots each) per throw
cargo run -- -n 10000000 -t 4 8d10

# default number of dice throws on 2 threads with default dice
cargo run -- -t 2
```
